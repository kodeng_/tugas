import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  SafeAreaView,
  Alert,
} from "react-native";
import { Button, Input } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome5";
import axios from "axios";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      email: "",
      phoneNumber: "",
      password: "",
      passwordConfirmation: "",
      storeToken: "",
    };
  }

  validationRegister = () => {
    const {
      name,
      username,
      email,
      phoneNumber,
      password,
      passwordConfirmation,
    } = this.state;
    const re = /\S+@\S+\.\S+/;
    if (name == "") {
      Alert.alert("Error!", "please fill your name");
    } else if (username == "") {
      Alert.alert("Error!", "Please fill username");
      return false;
    } else if (email == "") {
      Alert.alert("Error!", "Please fill email");
      return false;
    } else if (!re.test(email)) {
      Alert.alert("Error!", "Your email does't email format");
      return false;
    } else if (phoneNumber == "") {
      Alert.alert("Error!", "Please fill phone number");
      return false;
    } else if (password == "") {
      Alert.alert("Error!", "Please fill password");
      return false;
    } else if (passwordConfirmation == "") {
      Alert.alert("Error!", "Please fill confirm password");
      return false;
    } else if (password != passwordConfirmation) {
      Alert.alert("Error!", "Password and confirm password is not match");
      return false;
    }
    return true;
  };

  successRegister = async () => {
    if (this.validationRegister()) {
      axios
        .post("https://mighty-eyrie-29994.herokuapp.com/api/auth/signup", {
          name: this.state.name,
          username: this.state.username,
          email: this.state.email,
          password: this.state.password,
          passwordConfirmation: this.state.passwordConfirmation,
        })
        .then((res) => {
          // console.log(res);
          console.log(res.data);
          this.setState(res.data);
          Alert.alert("Holla!", "Successfully Register");
          this.props.navigation.navigate("Login");
        })
        .catch(function (error) {
          console.log(error);
          Alert.alert(error + "", "Your email or username is already there");
        });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ justifyContent: "center" }}>
          <View>
            <Text style={styles.title}>
              <Image
                source={require("../assets/sandninidrink.png")}
                resizeMode="center"
                style={styles.image}
              />
              Let's Get Started
            </Text>
            <Text style={styles.body}>To be Wargi Sandnini</Text>
            <Input
              placeholder="Your Name"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ name: value })}
              leftIcon={<Icon name="smile" size={20} color="grey" />}
              a
            />
            <Input
              placeholder="Username"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ username: value })}
              leftIcon={<Icon name="camera" size={20} color="grey" />}
              autoCapitalize="none"
            />

            <Input
              placeholder="Email"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ email: value })}
              leftIcon={<Icon name="user" size={20} color="grey" />}
              autoCapitalize="none"
            />
            <Input
              placeholder="Phone Number"
              keyboardType="numeric"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ phoneNumber: value })}
              leftIcon={<Icon name="phone" size={20} color="grey" />}
            />
            <Input
              placeholder="Password"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ password: value })}
              leftIcon={<Icon name="lock" size={20} color="grey" />}
              secureTextEntry={true}
            />
            <Input
              placeholder="Confirm Password"
              style={styles.inputs}
              onChangeText={(value) =>
                this.setState({ passwordConfirmation: value })
              }
              leftIcon={<Icon name="unlock" size={20} color="grey" />}
              secureTextEntry={true}
            />
            <Button
              title=" Register "
              onPress={() => this.successRegister()}
              style={{ flexDirection: "row", width: "90%" }}
              icon={<Icon name="check" size={15} color="white" />}
            />
            <Text
              onPress={() => this.props.navigation.navigate("Login")}
              style={{ color: "blue", textAlign: "center", marginTop: 10 }}
            >
              Already have an account
            </Text>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "moccasin",
    justifyContent: "center",
  },
  image: {
    width: 50,
    height: 50,
  },
  title: {
    fontSize: 40,
  },
  body: {
    textAlign: "center",
    fontSize: 20,
  },
  inputs: {
    width: "90%",
    height: 50,
    borderRadius: 20,
    marginVertical: 5,
    borderWidth: 1,
    fontWeight: "bold",
    borderColor: "grey",
    marginLeft: 5,
    padding: 10,
  },
});

export default Register;
