import * as React from "react";
import { StyleSheet, View } from "react-native";
import { Modal, Portal, Text, Button, Provider } from "react-native-paper";

const Profile = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Button
        style={{ width: "80%" }}
        onPress={() => navigation.navigate("Login")}
      >
        Logout
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    paddingTop: 10,
  },
});
export default Profile;
