import React, { Component, useEffect } from "react";
import "react-native-gesture-handler";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
} from "react-native";
import { Input, Button } from "react-native-elements";
import { IconButton, Colors } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/FontAwesome";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "admin",
      password: "12345678",
    };
  }

  loginValidation = () => {
    const { username, password } = this.state;
    if (username == "") {
      Alert.alert("Error!", "Please fill username");
      return false;
    } else if (password == "") {
      Alert.alert("Error!", "Please fill password");
      return false;
    }
    return true;
  };

  // useEffect = () => {
  //   setIsEnabled(state.username !== "" && state.password !== "" ? false : true);
  //   console.log(state.username);
  //   console.log(state.password);
  // };

  async storeToken(data) {
    try {
      await AsyncStorage.setItem("userData", data);
      console.log();
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }

  successLogin = async () => {
    if (this.loginValidation()) {
      axios
        .post("https://mighty-eyrie-29994.herokuapp.com/api/auth/signin", {
          username: this.state.username,
          password: this.state.password,
        })
        .then((res) => {
          // console.log(res);
          console.log(res.data);
          this.setState({
            token: res.data.accessToken,
          });
          this.storeToken(res.data.accessToken);
          console.log("ini token", res.data.accessToken);
          this.props.navigation.navigate("Dashboard", {
            screen: "Dashboard",
            params: {
              screen: "Dashboard",
              params: {
                usernameParams: this.state.username,
                passwordParams: this.state.password,
              },
            },
          });
        })
        .catch(function (error) {
          console.log(error);
          Alert.alert(error + "", "Your username and password does't match");
        });
    }
  };

  componentDidMount() {
    const _bootstrapAsync = async () => {
      const masuk = await AsyncStorage.getItem("userData");
      console.log("ini token", masuk);
      if (masuk !== null) {
        this.props.navigation.navigate("Dashboard", {
          screen: "Dashboard",
          params: {
            screen: "Dashboard",
          },
        });
      }
    };
    _bootstrapAsync();
  }

  state = { isFocused: false };

  onFocusChange = () => {
    this.setState({ isFocused: true });
  };

  render() {
    return (
      <View
        style={[
          styles.container,
          { borderColor: this.state.isFocused ? "black" : "grey" },
        ]}
      >
        <SafeAreaView style={{ justifyContent: "center" }}>
          <View>
            <Image
              source={require("../assets/sandnini.png")}
              resizeMode="center"
              style={styles.image}
            />
            <Text style={styles.title}>Welcome</Text>
            <Text style={styles.body}>Wargi Sandnini</Text>
            <Input
              placeholder="Username"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ username: value })}
              leftIcon={<Icon name="user" size={22} color="grey" />}
              autoCapitalize="none"
            />
            <Input
              placeholder="Password"
              style={styles.inputs}
              onChangeText={(value) => this.setState({ password: value })}
              secureTextEntry={true}
              leftIcon={
                <Icon
                  name="lock"
                  size={22}
                  color={this.state.isFocused ? "black" : "grey"}
                />
              }
            />
            <Button
              title=" LOGIN "
              type="solid"
              onPress={() => this.successLogin()}
              icon={<Icon name="home" size={15} color="white" />}
            />
            <Text style={{ textAlign: "center", marginTop: 10 }}>
              Don't have Account?{" "}
            </Text>

            <Text
              onPress={() => this.props.navigation.navigate("Register")}
              style={{ color: "blue", textAlign: "center" }}
            >
              Register
            </Text>
            <IconButton
              icon="camera"
              color={Colors.blue600}
              size={20}
              onPress={() =>
                this.props.navigation.navigate("Dashboard", {
                  screen: "Dashboard",
                  params: {
                    screen: "Dashboard",
                    params: {
                      usernameParams: "jalancepat",
                      passwordParams: "jepret",
                    },
                  },
                })
              }
            />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 40,
    textAlign: "center",
  },
  body: {
    fontSize: 20,
    textAlign: "center",
  },
  image: {
    width: 250,
    height: 250,
    justifyContent: "center",
  },
  inputs: {
    width: "90%",
    height: 50,
    borderRadius: 20,
    marginVertical: 5,
    borderWidth: 1,
    fontWeight: "bold",
    borderColor: "grey",
    marginLeft: 5,
    padding: 10,
    justifyContent: "center",
  },
});

export default Login;
