import React, { Component, useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Alert,
} from "react-native";
import { Input } from "react-native-elements";
import { Button } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/FontAwesome5";
import axios from "axios";

const Updates = ({ route, navigation }) => {
  const { rutId, rutName, rutUsername, rutEmail } = route.params;
  const [token, setToken] = useState("");
  const [roles, setRoles] = useState(null);
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");

  useEffect(() => {
    console.log("ini token : ", token);

    getToken();
  }, [token]);

  const getToken = async () => {
    try {
      setToken(await AsyncStorage.getItem("userData"));
      setRoles(await AsyncStorage.getItem("roles"));
      console.log("roket : " + token);
    } catch (e) {
      console.log("error = " + e);
    }
  };

  const update = () => {
    const header = {
      authorization: "Bearer " + token,
    };
    const body = {
      name: name,
      username: username,
      email: email,
    };

    axios
      .put(
        `https://mighty-eyrie-29994.herokuapp.com/api/users/${rutId}`,
        body,
        { headers: header }
      )
      .then(function (res) {
        // console.log("res = " + res.data);
        navigation.navigate("Users");
        Alert.alert("Yeay !", "Your data has been Updated");
      })
      .catch(function (error) {
        console.log(JSON.stringify(error.response, null, 1));
        alert(JSON.stringify(error.response, null, 1));
      });
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={{ justifyContent: "center" }}>
        <View style={{ marginTop: 10 }}>
          <Input
            label="Name"
            style={styles.inputs}
            onChangeText={(name) => setName(name)}
            defaultValue={rutName}
            //leftIcon={<Icon name="smile" size={20} color="grey" />}
          />
          <Input
            label="Username"
            style={styles.inputs}
            onChangeText={(username) => setUsername(username)}
            defaultValue={rutUsername}
            //leftIcon={<Icon name="camera" size={20} color="grey" />}
            autoCapitalize="none"
          />
          <Input
            label="Email"
            style={styles.inputs}
            onChangeText={(email) => setEmail(email)}
            defaultValue={rutEmail}
            //leftIcon={<Icon name="user" size={20} color="grey" />}
            autoCapitalize="none"
          />
          {/* <Input
            label="Password"
            style={styles.inputs}
            onChangeText={(password) => rutPassword(password)}
            defaultValue={rutPassword}
            // leftIcon={<Icon password="lock" size={20} color="grey" />}
            secureTextEntry={true}
          />
          <Input
            label="Confirm Password"
            style={styles.inputs}
            onChangeText={(password) => rutPassword(password)}
            defaultValue={rutPassword}
            leftIcon={<Icon name="unlock" size={20} color="grey" />}
            secureTextEntry={true}
          /> */}
          <Button
            style={{ padding: 3 }}
            mode="contained"
            onPress={() => update()}
          >
            Update
          </Button>
        </View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
  },
  inputs: {
    width: "90%",
    height: 50,
    borderRadius: 20,
    marginVertical: 5,
    borderWidth: 1,
    fontWeight: "bold",
    borderColor: "grey",
    marginLeft: 5,
    padding: 10,
  },
});

export default Updates;
