import React from "react";
import Navigation from "./components/Navigation";

const App: () => React$Node = () => {
  return <Navigation />;
};

export default App;
