import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  FlatList,
} from "react-native";
import { Modal, Portal, Text, Button, Provider } from "react-native-paper";
import Header from "./Header";

const Report = () => {
  const [visible, setVisible] = React.useState(false);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = { backgroundColor: "white", padding: 20 };

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  View,
    useEffect(() => {
      fetch("https://reactnative.dev/movies.json")
        .then((response) => response.json())
        .then((json) => setData(json.movies))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }, []);

  return (
    <View style={styles.container}>
      <Header title="Report" />

      <Modal
        visible={visible}
        onDismiss={hideModal}
        contentContainerStyle={containerStyle}
      >
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <Text>
                {item.title}, {item.releaseYear}
              </Text>
            )}
          />
        )}
      </Modal>
      <View style={styles.buttom}>
        <Button style={{ width: "80%" }} onPress={showModal}>
          Show
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    marginTop: 30,
  },
  title: {
    fontSize: 40,
    textAlign: "center",
  },
  buttom: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    paddingTop: 10,
  },
});

export default Report;
