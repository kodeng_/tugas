import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
} from "react-native";
import { Button, Text, Title } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import Header from "./Header";

const Users = ({ navigation }) => {
  const [token, setToken] = useState(null);
  const [roles, setRoles] = useState(null);
  const [refreshing, setRefreshing] = useState(true);
  const [data, setData] = useState([
    {
      id: 0,
      name: "",
      username: "",
      email: "",
    },
  ]);

  const getToken = async () => {
    try {
      setToken(await AsyncStorage.getItem("userData"));
      setRoles(await AsyncStorage.getItem("roles"));
      // console.log("token =" + token);
    } catch (e) {
      console.log("error" + e);
    }
  };

  useEffect(() => {
    getToken();
    console.log("token = " + token);
    const postToken = async (e) => {
      const header = {
        "Content-Type": "application/json",
        Authorization: token,
      };
      axios
        .get("https://mighty-eyrie-29994.herokuapp.com/api/users", {
          headers: header,
        })
        .then(function (res) {
          // console.log(res);
          setRefreshing(false);
          setData(res.data.user);
        })
        .catch(function (error) {
          setRefreshing(false);
          console.log(error.response.data, null, 2);

          alert(error);
        });
    };
    if (token != null) {
      postToken();
    }
  }, [token && refreshing == true]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
  }, []);

  const deleteUsers = (id) => {
    console.log(token);
    const postToken = async (e) => {
      const header = {
        "Content-Type": "application/json",
        Authorization: token,
      };
      axios
        .delete(`https://mighty-eyrie-29994.herokuapp.com/api/users/${id}`, {
          headers: header,
        })
        .then(function (res) {
          console.log(res.data.user);
          setData(res.data.user);
        })
        .catch(function (error) {
          console.log(JSON.stringify(error.response.data, null, 2));
          alert(error);
        });
    };
    if (token != null) {
      postToken();
    }
  };

  // const adminButton = (id, name, username, email) => {
  //   if (roles == "ADMIN") {
  //     return (
  //       <View>
  //         <Button
  //           style={styles.button}
  //           mode="outlined"
  //           onPress={() =>
  //             navigation.navigate("Updates", {
  //               rutId: id,
  //               rutName: name,
  //               rutUsername: username,
  //               rutEmail: email,
  //             })
  //           }
  //         >
  //           fuck
  //         </Button>
  //       </View>
  //     );
  //   }
  // };

  return (
    <View style={styles.container}>
      <Header title="Users" />
      <FlatList
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        horizontal={false}
        data={data}
        keyExtractor={({ username }, index) => username}
        renderItem={({ item }) => (
          <ScrollView
            style={{
              padding: 10,
              borderBottomColor: "darkslateblue",
              borderBottomWidth: 1,
            }}
          >
            <Text style={styles.list}>
              Name{"         "}: {item.name}
            </Text>
            <Text style={styles.list}>Username : {item.username}</Text>
            <Text style={styles.list}>
              Email{"          "}: {item.email}
            </Text>
            <Button
              style={styles.button}
              mode="outlined"
              onPress={() =>
                navigation.navigate("Updates", {
                  rutId: item.id,
                  rutName: item.name,
                  rutUsername: item.username,
                  rutEmail: item.email,
                })
              }
            >
              Update
            </Button>
            <Button
              style={styles.button}
              mode="outlined"
              onPress={() => deleteUsers(item.id)}
            >
              Delete
            </Button>
          </ScrollView>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    marginTop: 30,
    justifyContent: "center",
  },
  text: {
    textAlign: "center",
  },
  list: {
    justifyContent: "center",
    alignContent: "center",
  },
  button: {
    margin: 5,
  },
});
export default Users;
