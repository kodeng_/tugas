import "react-native-gesture-handler";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/FontAwesome5";

import Register from "../auth/Register";
import Login from "../auth/Login";
import Logout from "../auth/Logout";
import Updates from "../auth/Updates";
import Dashboard from "./Dashboard";
import Users from "./Users";
import Report from "./Report";
import Profile from "./Profile";
// import { Register, Login, Update, Logout } from "../auth"

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const TabScreen = () => (
  <Tab.Navigator tabBarOptions={{ style: { paddingBottom: 10 } }}>
    <Tab.Screen
      name="Dashboard"
      component={Dashboard}
      options={{ tabBarIcon: () => <Icon name="home" /> }}
    />
    <Tab.Screen
      name="Users"
      component={Users}
      options={{ tabBarIcon: () => <Icon name="users" /> }}
    />
    <Tab.Screen
      name="Profile"
      component={Profile}
      options={{ tabBarIcon: () => <Icon name="smile" /> }}
    />
  </Tab.Navigator>
);

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen
          name="Dashboard"
          component={DrawerDashboard}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Updates" component={Updates} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const DrawerDashboard = () => (
  <Drawer.Navigator>
    <Drawer.Screen
      name="Dashboard"
      component={TabScreen}
      Icon={<Icon name="home" />}
    />
    <Drawer.Screen name="Report" component={Report} />
    <Drawer.Screen name="Logout" component={Logout} />
  </Drawer.Navigator>
);

export default Navigation;
