import AsyncStorage from "@react-native-community/async-storage";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  Provider,
  Portal,
  ActivityIndicator,
  Alert,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { Modal, Text, Title, Button } from "react-native-paper";
import Header from "./Header";

const Dashboard = (props) => {
  const { navigation } = props;
  // const { usernameParams, passwordParams } = props.route.params;

  const [visible, setVisible] = React.useState(false);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = { backgroundColor: "white", padding: 20 };

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(
      "https://api.themoviedb.org/3/movie/popular?api_key=ecb397c7dcb6907a1424ccd887cd8277&language=en-US&page=1"
    )
      .then((respone) => respone.json())
      .then((json) => setData(json.results))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  successLogout = () => {
    asyncStorage.clear("userData");
    navigation.navigate("Login");
    Alert.alert("Nice to meet you!", "Successfully Logout.");
  };

  return (
    <View style={styles.container}>
      <View stle={styles.header}>
        <Header title="Dashboard" />
        {/* <Text style={styles.text}>Username : {usernameParams}</Text>
        <Text style={styles.text}>Password : {passwordParams}</Text> */}
      </View>
      <View>
        <FlatList
          horizontal={true}
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            <ScrollView>
              <TouchableOpacity
                style={{
                  alignItems: "center",
                  width: 128,
                  padding: 10,
                  marginLeft: 30,
                }}
              >
                <Image
                  style={styles.gambar}
                  source={{
                    uri: "https://image.tmdb.org/t/p/w500" + item.poster_path,
                  }}
                />
                <Title numberOfLines={1}>{item.original_title}</Title>
                <Text
                  style={{
                    justifyContent: "center",
                    alignContent: "center",
                    textAlign: "center",
                  }}
                  numberOfLines={3}
                >
                  {item.overview}
                </Text>
              </TouchableOpacity>
            </ScrollView>
          )}
        />
      </View>
      {/* <Provider>
        <Portal>
          <Modal
            visible={visible}
            onDismiss={hideModal}
            contentContainerStyle={containerStyle}
          ></Modal>
        </Portal>
      </Provider> */}
      <View style={styles.buttom}>
        <Button
          mode="contained"
          style={{ width: "80%" }}
          onPress={() => successLogout()}
        >
          Logout
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    marginTop: 30,
  },
  header: {
    flex: 1,
    backgroundColor: "black",
  },
  conttent: {
    flex: 1,
    backgroundColor: "moccasin",
  },
  buttom: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    paddingTop: 10,
  },
  text: {
    textAlign: "center",
  },
  title: {
    fontSize: 40,
    textAlign: "center",
  },
  gambar: {
    flex: 1,
    width: 150,
    height: 200,
    borderRadius: 28,
    marginLeft: 28,
    marginRight: 50,
  },
});
export default Dashboard;
