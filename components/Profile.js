import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { Text, Title, Input } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";
import Header from "./Header";

const Profile = ({ title }) => {
  return (
    <View style={styles.container}>
      <Header title="Profile" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "moccasin",
    marginTop: 30,
  },
});

export default Profile;
